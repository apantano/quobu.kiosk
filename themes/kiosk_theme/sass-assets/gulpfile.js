var gulp = require('gulp');
var sass = require('gulp-sass');
//var neat = require('node-neat').include_paths;
//var bourbon = require('node-bourbon').includePaths;
var livereload = require('gulp-livereload');


//var config = {
//    bowerDir: './bower_components'
//};


// Sass
gulp.task('sass', function() {
    return gulp.src('scss/gulp-style.scss')
        .pipe(sass({
            outputStyle: 'expanded',
            sourceComments: 'normal',
            errLogToConsole: true

        }))
        .pipe(gulp.dest('css'));
});

//Parse js files
gulp.task('scripts', function(){
    return gulp.src('js/*.js')
        .pipe(gulp.dest('js/min'));
});


// Watch Files For Changes
gulp.task('watch', function() {
    livereload.listen();

    gulp.watch(['scss/**/*.scss', 'scss/**/*.sass'], ['sass']);
    gulp.watch(['js/*.js'], ['scripts']);

    gulp.watch(['css/**']).on('change', livereload.changed);
    gulp.watch(['../templates/**']).on('change', livereload.changed);
});

// Default Task
gulp.task('default', ['sass', 'watch']);
