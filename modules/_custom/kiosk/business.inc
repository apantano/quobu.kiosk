<?php

define('RESTO_ENTITY_NOT_UPDATED', 0);
define('RESTO_ENTITY_UPDATED', 1);
define('RESTO_ENTITY_CREATED', 2);
define('RESTO_ENTITY_ERROR', 3);


/**
 * Create or Update Business nodes from resto.be webservice
 */


/**
 * Processes Importing a list of Businesess Nodes
 * @param $context
 * @return int
 */
function resto_process_businesses_nodes(&$context = NULL) {

  $track = array(0 =>0 , 1 => 0, 2 => 0, 3 => 0);
  $count = 0;

  //Import/Updates Business Nodes
  watchdog('Resto.be', 'Businesses importing process from resto.be started.', NULL, WATCHDOG_INFO);

  //Get all entities
  $entities = resto_get_remote_enitites();
  $businesses = $entities['entities'];
  $total = $entities['count'];

  foreach ($businesses as $business) {
    $t = resto_process_business_node($business);
    $track[$t]++;
    $count++;
  }

  watchdog('Resto.be', 'Business importing process from resto.be completed.', NULL, WATCHDOG_INFO);


  $result = array(
    'total' => $total,
    'count' => $count,
    'updated' => $track[RESTO_ENTITY_UPDATED],
    'untouched' => $track[RESTO_ENTITY_NOT_UPDATED],
    'created' => $track[RESTO_ENTITY_CREATED],
    'error' => $track[RESTO_ENTITY_ERROR],
  );

  if($context)
    $context['results']['businesses'] = $result;

  return $result;

}


function resto_get_remote_enitites() {
  $page = 1;
  $result = 0;
  $result_per_page = $total = 8;
  $businesses = array();
  $city = variable_get(KIOSK_RESTO_STARTING_CITY_SEARCH, 'Aalst');

  if(!$city){
    watchdog('Info', 'Businesses from Resto.be not imported because no city/location has been set.', NULL, WATCHDOG_INFO);
    drupal_exit();
  }

  while($result < $total){
    $request = resto_business_search(1, array(
      'location' => $city,
      'business_type' => 1,
      'resultsPerPage' => $result_per_page,
      'pageNumber' => $page,
    ));

    $total = (int)$request['totalResults'];
    $page++;
    $result += count($request['businesses']['business']);
    $businesses = array_merge($businesses, $request['businesses']['business']);
  }

  return array(
    'entities' => $businesses,
    'count' => $result,
  );

}


/**
 *
 * @param $item
 * @return int -
 * 0: Node already stored. Not updated.
 * 1: A new Business node is created.
 * 2: Node already stored but updated
 * 3: Node creating error
 */
function resto_process_business_node($item){


  $sha1 = sha1(serialize($item));
  $bid = $item['businessId'];

  //check if business already exists
  $check = _load_business_by_id($bid);

  if($check){
    //business already stored.
    $wrapper = entity_metadata_wrapper('node', $check);
    $stored_sha1 = getFieldValue($wrapper, "field_sha1");

    //compare the SHA1 values to see if they are the same version.
    if($sha1 == $stored_sha1){
      //The business node is alredy stored and does not need to be updated.
      return RESTO_ENTITY_NOT_UPDATED;

    }
    else{
      //The node need to be updated
      $nid = _resto_create_business_node($item, $sha1, TRUE);
      if($nid) {
        return RESTO_ENTITY_UPDATED;
      }
      else {
        return RESTO_ENTITY_ERROR;
      }
    }

  }
  else{
    //Business node not stored in the database. We need to create it.
    $nid = _resto_create_business_node($item, $sha1);
    if($nid) {
      return RESTO_ENTITY_CREATED;
    }
    else {
      return RESTO_ENTITY_ERROR;
    }
  }

}

function _resto_create_business_node($business, $sha1, $update = FALSE) {

  if(!$update) {
    $values = array(
      'type' => 'business',
      'uid' => 0,
      'status' => 1,
      'comment' => 0,
      'promote' => 0,
    );

    $entity = entity_create('node', $values);
  }
  else{
    $entity = _load_business_by_id($business['businessId']);
    if(!$entity){
      return FALSE;
    }
  }

  try {

    $wrapper = entity_metadata_wrapper('node', $entity);


    $wrapper->title->set(valueOfOrDefault($business, 'businessName'));
    $wrapper->field_business_id->set(valueOfOrDefault($business, 'businessId'));
    $wrapper->field_type_id->set(valueOfOrDefault($business, 'businessTypeId'));
    $wrapper->field_business_street->set(valueOfOrDefault($business, 'street'));
    $wrapper->field_box_number->set(valueOfOrDefault($business, 'boxNumber'));
    $wrapper->field_zip->set(valueOfOrDefault($business, 'zip'));
    $wrapper->field_city->set(valueOfOrDefault($business, 'city'));
    $wrapper->field_region->set(valueOfOrDefault($business, 'region'));
    $wrapper->field_business_phone->set(valueOfOrDefault($business, 'phone'));
    $wrapper->field_business_fax->set(valueOfOrDefault($business, 'fax'));
    $wrapper->field_mobile_phone->set(valueOfOrDefault($business, 'mobilePhone'));

    //Cuisines
    $cuisines = valueOfOrDefault($business, 'cuisines::cuisine');
    $cuisine_array = array();

    if(is_array($cuisines)){
      foreach ($cuisines as $cuisine) {
        $terms = taxonomy_get_term_by_name($cuisine, 'cuisine');
        foreach ($terms as $t) {
          $cuisine_array[] = $t->tid;
        }
      }
    }
    else {
      $terms = taxonomy_get_term_by_name($cuisines, 'cuisine');
      if ($terms) {
        foreach($terms as $t){
          $cuisine_array[] = $t->tid;
        }

      }
    }
    $wrapper->field_cousine->set($cuisine_array);


    //Url
    $wrapper->field_business_url->set(valueOfOrDefault($business, 'url'));

    //Map Coordinates
    if(($lat = valueOfOrDefault($business, 'lat')) && ($lon = valueOfOrDefault($business, 'lon'))){
      $coordinates = array(
        'lat' => valueOfOrDefault($business, 'lat'),
        'lon' => valueOfOrDefault($business, 'lon')
      );

      $origin_lat = variable_get(KIOSK_LATITUDE);
      $origin_lon = variable_get(KIOSK_LONGITUDE);

      $distance = kiosk_map_direction_request(variable_get(KIOSK_GOOGLE_API_KEY), $origin_lat . ',' . $origin_lon, $lat . ',' . $lon);

      if(!empty($distance) && isset($distance['walking']) && $distance['walking']->status == 'OK') {
        $distance_value = $distance['walking']->routes[0]->legs[0]->distance->value;
        $distance_text  = $distance['walking']->routes[0]->legs[0]->distance->text;

        if($distance_value){
          $wrapper->field_distance_value->set($distance_value);
        }

        if($distance_text){
          $wrapper->field_distance_text->set($distance_text);
        }
      }


      $wrapper->field_business_coordinates->set($coordinates);
    }


    //Ratings
    $wrapper->field_avgrating1->set(valueOfOrDefault($business, 'avgRating1'));
    $wrapper->field_avgrating2->set(valueOfOrDefault($business, 'avgRating2'));
    $wrapper->field_avgrating3->set(valueOfOrDefault($business, 'avgRating3'));
    $wrapper->field_avgratingcombined->set(valueOfOrDefault($business, 'avgRatingCombined'));
    $wrapper->field_nrofratings->set(valueOfOrDefault($business, 'nrOfRatings'));

    //Flags
    $wrapper->field_isnew->set(valueOfOrDefault($business, 'isNew', FALSE));
    $wrapper->field_ispremium->set(valueOfOrDefault($business, 'isPremium'));
    $wrapper->field_istopresult->set(valueOfOrDefault($business, 'isTopResult'));

    //Stars
    $wrapper->field_business_stars->set(valueOfOrDefault($business, 'stars', NULL));

    //Description
    $description = valueOfOrDefault($business, 'description', '');
    if($description) {
      $wrapper->field_description->set(
        array(
          'value' => $description,
          'format' => 'general',
        ));
    }

    //Closing Days
    $wrapper->field_closingdays->set(valueOfOrDefault($business, 'closingDays'));


    //TODO: Implemnets Holiday?
    //    $wrapper->field_business_holidays->set(array(
    //      'value' => valueOfOrDefault($business, 'holiday::startDate'),
    //      'value2' => valueOfOrDefault($business, 'holiday::endDate'),
    //    ));


    //Budgets
    $budget_id = valueOfOrDefault($business, 'budgetId');
    if($budget_id) {
      $price = taxonomy_term_load(get_tid_by_restoId('budget', $budget_id));
      $wrapper->field_price->set($price->name);
      $wrapper->field_budget->set($price->tid);
    }

    //Payments
    $payments = valueOfOrDefault($business, 'paymentTypeIds::paymentTypeId');
    $pays = array();
    if(is_array($payments)) {
      foreach ($payments as $payment) {
        $pays[] = get_tid_by_restoId('payment', $payment);
      }
      $wrapper->field_payment_types->set($pays);
    }
    else{
      $pays[] = get_tid_by_restoId('payment', $payments);
      $wrapper->field_payment_types->set($pays);
    }

    //Pictures
    $images = valueOfOrDefault($business, 'pictures::picture');

    if(!empty($images)) {

      if (is_array($images)) {
        foreach ($images as $i) {
          $file = _resto_save_file($i);
          $wrapper->field_business_pictures[] = array('fid' => $file->fid);
        }
      }
      else {
        $file = _resto_save_file($images);
        $wrapper->field_business_pictures[] = array('fid' => $file->fid);
      }
    }


    //SHA1
    $wrapper->field_sha1->set($sha1);

    //Save the node.
    $wrapper->save();

    return $wrapper->getIdentifier();
  }
  catch (Exception $e) {
    watchdog('Resto.be', 'Error importing one business entity from Resto.be. </br>' . $e->getMessage(), NULL, WATCHDOG_INFO);
    return NULL;
  }

}

function _resto_save_file($file_path) {

  $scheme = file_default_scheme() . '://';
  $basename = basename($file_path);

  $file = system_retrieve_file($file_path, $scheme . $basename, TRUE, FILE_EXISTS_REPLACE); // Create a File object

  return $file;
}


/**
 * Create taxonomies
 * @param $context
 * @return array of counts
 */
function resto_process_services_taxonomies(&$context = NULL){

  $count = 0;
  $c = $p = $b = 0;
  //Import/update Taxonomies
  watchdog('Resto.be', 'Taxonomies importing process from resto.be started.', NULL, WATCHDOG_NOTICE);

  $cousines = resto_search_parameters(1, 'CUISINE');
  $payments = resto_search_parameters(1, 'PAYMENT');
  $budgets = resto_search_parameters(1, 'BUDGET');

  foreach ($cousines['searchParameters']['searchParameter'] as $cuisine) {
    _resto_create_taxonomy_terms_from_resto_service($cuisine, 'cuisine');
    $count++;
    $c++;
  }

  foreach ($payments['searchParameters']['searchParameter'] as $payment) {
    _resto_create_taxonomy_terms_from_resto_service($payment, 'payment');
    $count++;
    $p++;
  }

  foreach ($budgets['searchParameters']['searchParameter'] as $budget) {
    _resto_create_taxonomy_terms_from_resto_service($budget, 'budget');
    $count++;
    $b++;
  }

  watchdog('Resto.be', 'Taxonomies importing process from resto.be completed.', NULL, WATCHDOG_NOTICE);

  $result = array(
    'total' => $count,
    'cuisines' => $c,
    'payments' => $p,
    'budgets' => $b,
  );

  if($context)
    $context['results']['services'] = $result;

  return $result;



}


/**
 * @param $id The business ID to search.
 * @return Return the Node ID (nid) or NULL.
 */
function _load_business_by_id($id) {

  $query = new EntityFieldQuery();

  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'business')
    ->fieldCondition('field_business_id', 'value', $id);

  $result = $query->execute();

  if(isset($result['node'])) {
    $nodes =array_keys($result['node']);
    return $nodes[0];
  }

  return NULL;
}

/**
 * @param $bundle - cousine, budget, payment
 * @param $rid - RestoID
 * @return null or the taxonomy term id (tid)
 */
function get_tid_by_restoId($bundle, $rid) {

  $query = new EntityFieldQuery();

  $query->entityCondition('entity_type', 'taxonomy_term')
    ->entityCondition('bundle', $bundle)
    ->fieldCondition('field_resto_id', 'value', $rid);

  $result = $query->execute();

  if(isset($result['taxonomy_term'])) {
    $taxonomies = array_keys($result['taxonomy_term']);
    return $taxonomies[0];
  }

  return NULL;


}

/**
 * Return an array of Terms ID from a list of Resto.be services
 *
 * @param $bundle
 * @param $list
 * @return array of tid from a list of services ID.
 */
function get_tid_array_by_service($list){

  $tids = array();

  foreach ($list as $k => $v) {
    $term = taxonomy_get_term_by_name($v);

    if($term) {
      foreach ($term as $t) {
        $tids[] = $t->tid;
      }
    }
  }

  return $tids;
}


/**
 * Create Taxonomy terms from resto-be services (cuisine, budget, payment)
 *
 * @param $service
 * @param $bundle
 * @return TermId (tid)
 */
function _resto_create_taxonomy_terms_from_resto_service($service, $bundle){
  $rid = $service['id'];
  $wrapper = NULL;
  if($tid = get_tid_by_restoId($bundle, $rid)){
    //Update
    try {
      $wrapper = entity_metadata_wrapper('taxonomy_term', $tid);
      $name = $service['name'];
      if($bundle == 'budget') {
        $name = preg_replace("/[^0-9]/","",$name);
      }
      $wrapper->name->set($name);
      $wrapper->field_resto_id->set($rid);
      $wrapper->save();
    }
    catch(Exception $ex){
      watchdog('Resto.be', "Error updating taxonomy term ${tid}. " . $ex->getMessage(),NULL, WATCHDOG_ERROR);
      return NULL;
    }
  }
  else {
    //create
    $v = taxonomy_vocabulary_machine_name_load($bundle);

    $data = array(
      'name' => $service['name'],
      'vid' => $v->vid,
      'parent' => array(0),
    );

    $term = entity_create('taxonomy_term', $data);
    $wrapper = entity_metadata_wrapper('taxonomy_term', $term);
    $wrapper->save();
    $wrapper->field_resto_id->set($rid);
    $wrapper->save();
  }

  if($wrapper){
    return $wrapper->getIdentifier();
  }

  return NULL;
}