<?php

define('KIOSK_RESTO_API_DOMAIN', 'http://resto.be/api/');

include_once(drupal_get_path('module', 'kiosk') . '/business.inc');

/**
 * Rest.be APIs
 */


function resto_execute_import() {


  //Import/update Taxonomies
  resto_process_services_taxonomies();

  //Import/Update Businesses
  resto_process_businesses_nodes();


}


// *********************  BUSINESS ***********************//

function resto_get_business_details($business_id){
  $args = array(
    'businessId' => $business_id,
    'language' => variable_get(KIOSK_RESTO_LANGUAGE, 'EN'),
  );

  return resto_be_method_call('business/getDetails', $args);
}

function resto_get_menu_titles($business_id){
  $args = array(
    'businessId' => $business_id,
    'language' => variable_get(KIOSK_RESTO_LANGUAGE, 'EN'),
  );

  return resto_be_method_call('business/getMenuTitles', $args);
}

function resto_get_menu_content($business_id, $menu_type_id){
  $args = array(
    'businessId' => $business_id,
    'language' => variable_get(KIOSK_RESTO_LANGUAGE, 'EN'),
    'menuTypeId' => $menu_type_id,
  );

  return resto_be_method_call('business/getMenu', $args);
}

function resto_get_business_promotions($business_id){
  $args = array(
    'businessId' => $business_id,
    'language' => variable_get(KIOSK_RESTO_LANGUAGE, 'EN'),
  );

  return resto_be_method_call('business/promotions', $args);
}
//----------> BUSINESS



// ******************** SERVICES *************************//
function resto_business_search($business_type, $criteria){
  $args = array(
    'businessType' => $business_type,
    'language' => variable_get(KIOSK_RESTO_LANGUAGE, 'EN'),
  );

 $args = array_merge($args, $criteria);

  return resto_be_method_call('search/business', $args);
}

function resto_business_name_suggestion($full_text, $business_type){
  $args = array(
    'fullText' => $full_text,
    'businessType' => $business_type,
    'language' => variable_get(KIOSK_RESTO_LANGUAGE, 'EN'),
  );

  return resto_be_method_call('business/suggestion', $args);
}

function resto_city_name_suggestion($full_text, $business_type){
  $args = array(
    'fullText' => $full_text,
    'businessType' => $business_type,
    'language' => variable_get(KIOSK_RESTO_LANGUAGE, 'EN'),
  );

  return resto_be_method_call('search/city/suggestion', $args);
}

function resto_search_parameters($business_type, $data_type){
  $args = array(
    'businessType' => $business_type,
    'dataType' => $data_type,
    'language' => variable_get(KIOSK_RESTO_LANGUAGE, 'EN'),
  );

  return resto_be_method_call('search/parameters', $args);
}

//----->>SERVICES



//GENERIC METHOD CALL
/**
 * Generic Resto.be Method Call
 *
 * @param $method
 * @param $args
 * @return mixed
 */
function resto_be_method_call($method, $args) {

  $base = KIOSK_RESTO_API_DOMAIN . $method . '/v1?';

  $args['apiToken'] =  variable_get(KIOSK_RESTO_API_TOKEN);
  $args['clientsOnly'] =false;

  // Initialize cURL
  $curlHandle = curl_init();

  // Configure cURL request
  curl_setopt($curlHandle, CURLOPT_URL, $base);

  // Configure POST
  curl_setopt($curlHandle, CURLOPT_POST, 1);
  $args = http_build_query($args);
  curl_setopt($curlHandle, CURLOPT_POSTFIELDS, $args);

  // Make sure we can access the response when we execute the call
  curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);

  // Execute the API call
  $xmlEncodedApiResponse = curl_exec($curlHandle);

  // Ensure HTTP call was successful
  if($xmlEncodedApiResponse === false) {
    throw new \RuntimeException(
      'API call failed with cURL error: ' . curl_error($curlHandle)
    );
  }

  // Clean up the resource now that we're done with cURL
  curl_close($curlHandle);

  $xml = simplexml_load_string($xmlEncodedApiResponse);
  $json = json_encode($xml);

  // Decode the response from a JSON string to a PHP associative array
  $apiResponse = json_decode($json, true);

  // Make sure we got back a well-formed JSON string and that there were no
  // errors when decoding it
  $jsonErrorCode = json_last_error();
  if($jsonErrorCode !== JSON_ERROR_NONE) {
    throw new \RuntimeException(
      'API response not well-formed (json error code: ' . $jsonErrorCode . ')'
    );
  }

  return $apiResponse;
}