(function ($) {
    Drupal.behaviors.kiosk = {
        attach: function (context, settings) {

            var FSizeCookie = 'kiosk-sizer';
            var $sizer = $('.font-sizer');
            var $sizeCookie = $.cookie(FSizeCookie);

            if($sizeCookie) {
                $('body').css('font-size', $sizeCookie + 'px');
                $sizer.find('span').removeClass('active');
                $sizer.find('span[data-size=' + $sizeCookie + ']').addClass('active');
            }

            $sizer.on('click', function(e){
                $(this).find('span').removeClass('active');
                $(e.target).addClass('active');
                var $size = $(e.target).attr('data-size');
                console.log($size);
                $.cookie(FSizeCookie, $size, { expires: 7, path: '/' });
                $('body').css('font-size', $size + 'px');
            });
        }
    }
})(jQuery);
