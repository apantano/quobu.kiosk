function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}


/**
 * Add the viewportOffset function jQuery.
 */
(function($){
    '$:nomunge'; // Used by YUI compressor.

    var win = $(window);

    $.fn.viewportOffset = function() {
        var offset = $(this).offset();

        if(offset==null) return null;

        return {
            left: offset.left - win.scrollLeft(),
            top: offset.top - win.scrollTop()
        };
    };

})(jQuery);

var removeElements = function(text, selector) {
    var wrapped = jQuery("<div>" + text + "</div>");
    wrapped.find(selector).remove();
    return wrapped.html();
};


(function ($) {
    Drupal.behaviors.gulp_asset = {
        attach: function (context, settings) {

            $('img').removeAttr('alt').removeAttr('title');


            var attractionLinks = $('.child-attraction-node-last .attraction-nav a');
            var hasActive = false;

            attractionLinks.each(function(){
                if($(this).hasClass('active')){
                    hasActive = true;
                }
            });

            if (!hasActive) {
                attractionLinks.first().addClass('active');
            }

            $('.attraction-info .expander').click(function(){
                $(this).closest('.attraction-info').toggleClass('collapsed');
            });

            var queryStrings = getUrlVars();

            var qs = queryStrings['mode'];

            if (qs){
                $('.attraction-directions a[href*=' + qs + ']').addClass('selected');
            }
            else{
                $('.attraction-directions .walking a').addClass('selected');
            }


            $('.scrollable-content, .pane-penultimate-attractions-list-panel-pane-1, .caption-text, .bef-select-as-links > .form-item, .resto-view .view-content, .business-info .description')
                .mCustomScrollbar({
                theme:"light-3",
                scrollButtons: {
                    enable: true
                }
            });

            /**
             * FOTO'S PAGE
             * Select the slides count and move it inside the foto's link
             */

            var $slideCounter = $('.page-node-fotos .views-slideshow-slide-counter');
            if($slideCounter.length > 0) {
                $slideCounter.html($slideCounter.html().replace(/\of/g, '/'));
                var $fotosLink = $('.attraction-nav a[href$=fotos]');
                var counter = $("<span class='counter'></span>");
                counter.html($slideCounter)
                var linkText = $("<span class='link'>" + $fotosLink.text() + "</span>");
                linkText.append(counter);
                $fotosLink.html(linkText);
            }



        }
    }
})(jQuery);
